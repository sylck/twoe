#!/usr/bin/env python


# 3rd party modules
import libtcodpy as libtcod
import pygame

# game files
import constants


#             __                        __
#     _______/  |________ __ __   _____/  |_  ______
#    /  ___/\   __\_  __ \  |  \_/ ___\   __\/  ___/
#    \___ \  |  |  |  | \/  |  /\  \___|  |  \___ \
#   /____  > |__|  |__|  |____/  \___  >__| /____  >
#        \/                          \/          \/
#
class struc_Tile:
    def __init__(self, is_wall, is_void = False, is_heal = False):
        self.is_wall = is_wall
        self.is_void = is_void
        self.is_healpoint = is_heal


#         ___.        __               __
#     ____\_ |__     |__| ____   _____/  |_  ______
#    /  _ \| __ \    |  |/ __ \_/ ___\   __\/  ___/
#   (  <_> ) \_\ \   |  \  ___/\  \___|  |  \___ \
#    \____/|___  /\__|  |\___  >\___  >__| /____  >
#              \/\______|    \/     \/          \/
#
class obj_Actor:
    def __init__(self, x, y, name_object, sprite, creature = None, ai = None):
        self.x = x    # map address
        self.y = y    # map address
        self.sprite = sprite

        self.name_object = name_object

        self.creature = creature
        if creature:
            creature.owner = self

        self.ai = ai
        if ai:
            ai.owner = self


    def draw(self):
        SURFACE_MAIN.blit(
            self.sprite,
            (self.x * constants.CELL_WIDTH, self.y * constants.CELL_HEIGHT)
        )




#                                                            __
#     ____  ____   _____ ______   ____   ____   ____   _____/  |_  ______
#   _/ ___\/  _ \ /     \\____ \ /  _ \ /    \_/ __ \ /    \   __\/  ___/
#   \  \__(  <_> )  Y Y  \  |_> >  <_> )   |  \  ___/|   |  \  |  \___ \
#    \___  >____/|__|_|  /   __/ \____/|___|  /\___  >___|  /__| /____  >
#        \/            \/|__|               \/     \/     \/          \/
#
class com_Creature:
    '''
    Creatures have health, can damage other objects by attacking them. Can
    also be defeated.
    '''
    def __init__(self, name_instance, hp = 10, ap = 10, defeat_function = None):
        self.name_instance = name_instance
        self.maxhp = hp
        self.hp = hp
        self.defeat_function = defeat_function
        self.ap = ap

        self.in_fight = False
        self.enemy = None


    def hit_enemy(self):
        # smart caller needed, this accesses Noneful objects
        dmg = libtcod.random_get_int(0, 0, self.ap)
        self.attack(self.enemy, dmg)

    def run_away(self):
        self._destroy_fight()

    def move(self, dx, dy):
        # this works because the map is walled
        tile_is_wall = GAME_MAP[self.owner.x + dx][self.owner.y + dy].is_wall
        tile_is_healpoint = GAME_MAP[self.owner.x + dx][self.owner.y + dy].is_healpoint

        if not tile_is_wall and not self.in_fight:
            self.owner.x += dx
            self.owner.y += dy

            if tile_is_healpoint and self.name_instance == "Player":
                print "player healed!"
                self.hp = self.maxhp

        # move() is reached only if not in fight, so no need to check in_fight
        #  here
        # but I find stupid to fight while on the healpoint, because:
        #  a) it's unfair (hp refilled after every turn)
        #  b) it should be implemented as well
        if self.name_instance == "Player" and not tile_is_healpoint:
            # TODO: reduce spawn probability
            in_fight_int = libtcod.random_get_int(0, -20, 20)
            self.in_fight = in_fight_int >= 0

            if self.in_fight:
                print "player in fight, can't move now"

                if not self.enemy:
                    self._gen_enemy()



    def _gen_enemy(self):
        # let's spawn the enemy somewhere in the player's h/v neighbourhood
        #
        # we don't need to care about index out-of-bounds problems, we're far
        #  enough, even from the visible map edges
        # neither do we need to bother that the enemy will spawn in the void,
        #  as the playable area is surrounded with walls

        # enumerate potential spawn points:
        #  top, right, bottom, left

        diffs = [(0, -1), (1, 0), (0, 1), (-1, 0)]
        psp = [(self.owner.x + d[0], self.owner.y + d[1]) for d in diffs]

        # valid spawn points list
        vsp = []
        for coord in psp:
            cur_cell = GAME_MAP[coord[0]][coord[1]]
            if not cur_cell.is_wall and not cur_cell.is_void and not cur_cell.is_healpoint:
                vsp.append(coord)

        # now let's pick one of them randomly
        spawn_coords = vsp[libtcod.random_get_int(0, 0, len(vsp) - 1)]

        # spawn the enemy!
        enemy_creature = com_Creature("Enemy", ap = 1, defeat_function = defeat_monster)
        enemy_ai = ai_Test()

        self.enemy = obj_Actor(
            spawn_coords[0], spawn_coords[1],   # randomly picked x and y
            "troll",                            #  coordinates from vsp
            constants.S_ENEMY,
            creature = enemy_creature,
            ai = enemy_ai
        )

        GAME_OBJECTS.append(self.enemy)

    def _destroy_fight(self):
        # smart caller needed, this accesses Noneful objects
        enemy_objname = self.enemy.name_object

        for obj in GAME_OBJECTS:
            if obj.name_object == enemy_objname:
                GAME_OBJECTS.remove(obj)
                break

        self.in_fight = False
        self.enemy = None



    def attack(self, target, damage):
        print self.name_instance + " attacks " + target.creature.name_instance + " for " + str(damage) + " damage!"
        target.creature.take_damage(damage)

        # delete the enemy object from map if defeated
        # we'll come here only if it's an enemy, not the PLAYER
        if target.creature is None:
            print target.name_object + " is killed."

            self._destroy_fight()

            # increment killcount
            global ENEMIES_TO_DEFEAT
            ENEMIES_TO_DEFEAT -= 1
            if ENEMIES_TO_DEFEAT == 0:
                game_finish(True)





    def take_damage(self, damage):
        self.hp -= damage
        print self.name_instance + "'s health is " + str(self.hp) + "/" + str(self.maxhp)

        if self.hp <= 0:
            if self.defeat_function is not None:
                self.defeat_function(self.owner)
            else:
                # erm, it's a player then.
                game_finish(False)

# TODO: class com_Item:

# TODO: class com_Container:


#      _____  .___
#     /  _  \ |   |
#    /  /_\  \|   |
#   /    |    \   |
#   \____|__  /___|
#           \/
#
class ai_Test:
    '''
    Once per turn, execute.
    '''
    def take_turn(self):
        # random attack, 0 is miss, 1 is 1 HP hit
        self.owner.creature.attack(PLAYER, libtcod.random_get_int(0, 0, 10))


def defeat_monster(monster):
    ''' On defeat, most monsters stop moving. '''
    print monster.creature.name_instance + " is defeated!"
    monster.creature = None
    monster.ai = None

#     _____ _____  ______
#    /     \\__  \ \____ \
#   |  Y Y  \/ __ \|  |_> >
#   |__|_|  (____  /   __/
#         \/     \/|__|
#
def map_create():
    new_map = [
        [struc_Tile(False, True) for y in range(constants.MAP_HEIGHT)]
        for x in range(constants.MAP_WIDTH)
    ]

    # this code is suboptimal. but it does the trick
    is_wall = [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0],
               [0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0],
               [0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0],
               [0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0],
               [0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0],
               [0,0,0,0,0,1,1,1,1,1,0,1,1,1,1,1,0,0,0,0],
               [0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,1,1,1,1,1,0,1,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,1,0,1,1,1,0,1,1,1,1,1,1,1,1,0],
               [0,0,0,0,1,1,0,1,0,1,0,0,0,0,0,0,0,0,1,0],
               [0,0,0,0,1,0,0,1,0,1,0,1,1,1,1,0,0,0,1,0],
               [0,0,0,0,1,0,1,1,0,1,0,1,0,0,1,0,0,0,1,0],
               [0,0,0,0,1,0,1,0,0,1,0,1,0,0,1,1,1,1,1,0],
               [0,0,0,0,1,0,1,0,1,1,0,1,1,0,0,0,0,0,0,0],
               [0,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0,0,0],
               [0,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,0,0,0,0],
               [0,0,0,0,1,1,1,0,1,0,0,0,1,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0]]

    is_floor= [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0],
               [0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0],
               [0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0],
               [0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0],
               [0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,1,1,1,1,1,0,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,1,0,0,0,1,1,1,1,1,1,1,1,0,0],
               [0,0,0,0,0,1,1,0,0,0,1,0,0,0,0,1,1,1,0,0],
               [0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,1,1,0,0],
               [0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,1,0,0,0,1,1,1,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,1,0,0,0,1,1,1,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]]

    for y in range(constants.MAP_HEIGHT):
        for x in range(constants.MAP_WIDTH):
            if is_wall[x][y] == 1:
                # x and y swapped to make 2d matrix -> wall conversion easier.
                #  (otherwise, things get rotated 90deg ccw)
                new_map[y][x].is_wall = True
            elif is_floor[x][y] == 1:
                new_map[y][x].is_void = False

    new_map[10][17].is_healpoint = True

    return new_map

def map_check_for_creature(dest_x, dest_y, exclude_object = None):
    target = None

    if exclude_object:
        # check objectlist to find creature at that location that isn't excluded
        for obj in GAME_OBJECTS:
            # (1) don't evaluate the object itself
            # (2) object not self? check if move to the destination would
            #  result in an overlap
            # (3) no overlap? check if the object is a creature (and not an
            #  item or container)
            #
            # if the evaluated object is not self, will overlap and the
            #  colliding object is a creature, break and return the detected
            #  object
            if (
                obj is not exclude_object
                and obj.x == dest_x and obj.y == dest_y
                and obj.creature
            ):
                target = obj

            if target:
                return target

    else:
        # check objectlist to find any creature at that location
        for obj in GAME_OBJECTS:
            if (
                obj.x == dest_x and obj.y == dest_y
                and obj.creature
            ):
                target = obj

            if target:
                return target



#       .___                    .__
#     __| _/___________ __  _  _|__| ____    ____
#    / __ |\_  __ \__  \\ \/ \/ /  |/    \  / ___\
#   / /_/ | |  | \// __ \\     /|  |   |  \/ /_/  >
#   \____ | |__|  (____  /\/\_/ |__|___|  /\___  /
#        \/            \/               \//_____/
#
def draw_game():
    # using 'global SURFACE_MAIN' we access the global SURFACE_MAIN defined
    # elsewhere
    # no. we don't need this. we declared this thing elsewhere, and it's now
    # globally available.
    # global SURFACE_MAIN, GAME_MAP

    # clear the surface
    SURFACE_MAIN.fill(constants.COLOR_DEFAULT_BG)

    # draw the map
    draw_map(GAME_MAP)

    # draw all objects
    for obj in GAME_OBJECTS:
        obj.draw()

    p_hp = PLAYER.creature.hp
    p_mhp = PLAYER.creature.maxhp
    player_hp_str = "HP: " + str(p_hp) + "/" + str(p_mhp)
    enemies_str = "Kills: " + str(10 - ENEMIES_TO_DEFEAT) + "/" + str(10)
    player_hp_color = constants.COLOR_WHITE if p_hp > 10 else (255, 64, 0)

    SURFACE_FONT = pygame.font.SysFont('FreeSans', 12)

    # this should be a loop?
    SURFACE_FONT.set_bold(True)
    text_surf = SURFACE_FONT.render('Morgoth the Necromage', True, constants.COLOR_WHITE, (0, 0, 0))
    SURFACE_MAIN.blit(text_surf, (5, 5))
    SURFACE_FONT.set_bold(False)

    text_surf = SURFACE_FONT.render(player_hp_str, True, player_hp_color, (0, 0, 0))
    SURFACE_MAIN.blit(text_surf, (5, 20))

    text_surf = SURFACE_FONT.render(enemies_str, True, constants.COLOR_WHITE, (0, 0, 0))
    SURFACE_MAIN.blit(text_surf, (5, 35))

    if GAME_FINISHED:
        mainsurfrect = SURFACE_MAIN.get_rect()

        SURFACE_FONT.set_bold(True)

        if GAME_F_WIN:
            text_surf = SURFACE_FONT.render("Victory!", True, (0, 255, 0), (0, 0, 0))
        else:
            text_surf = SURFACE_FONT.render("Defeated!", True, (255, 0, 0), (0, 0, 0))

        tsrect = text_surf.get_rect()
        tsrect.right = mainsurfrect.right
        tsrect.bottom = mainsurfrect.bottom
        tsrect = tsrect.move(-5, -35)
        SURFACE_MAIN.blit(text_surf, tsrect)

        text_surf = SURFACE_FONT.render("restart [R]", True, constants.COLOR_WHITE, (0, 0, 0))
        tsrect = text_surf.get_rect()
        tsrect.right = mainsurfrect.right
        tsrect.bottom = mainsurfrect.bottom
        tsrect = tsrect.move(-5, -20)
        SURFACE_MAIN.blit(text_surf, tsrect)

        text_surf = SURFACE_FONT.render("quit [Q]", True, constants.COLOR_WHITE, (0, 0, 0))
        tsrect = text_surf.get_rect()
        tsrect.right = mainsurfrect.right
        tsrect.bottom = mainsurfrect.bottom
        tsrect = tsrect.move(-5, -5)
        SURFACE_MAIN.blit(text_surf, tsrect)
        SURFACE_FONT.set_bold(False)


    if PLAYER.creature.in_fight:
        text_surf = SURFACE_FONT.render("Ogre", True, (255, 0, 0), (0, 0, 0))
        tsrect = text_surf.get_rect()
        tsrect.right = SURFACE_MAIN.get_rect().right
        tsrect = tsrect.move(-5, 5)
        SURFACE_MAIN.blit(text_surf, tsrect)

        enemy_obj = PLAYER.creature.enemy
        enemy_c = enemy_obj.creature
        e_hp = enemy_c.hp
        e_mhp = enemy_c.maxhp
        e_hp_str = "HP: " + str(e_hp) + "/" + str(e_mhp)

        text_surf = SURFACE_FONT.render(e_hp_str, True, constants.COLOR_WHITE, (0, 0, 0))
        tsrect = text_surf.get_rect()
        tsrect.right = SURFACE_MAIN.get_rect().right
        tsrect = tsrect.move(-5, 20)
        SURFACE_MAIN.blit(text_surf, tsrect)

        SURFACE_FONT.set_bold(True)
        text_surf = SURFACE_FONT.render("hit [H]", True, constants.COLOR_WHITE, (0, 0, 0))
        tsrect = text_surf.get_rect()
        tsrect.right = SURFACE_MAIN.get_rect().right
        tsrect = tsrect.move(-5, 50)
        SURFACE_MAIN.blit(text_surf, tsrect)

        text_surf = SURFACE_FONT.render("run away [A]", True, constants.COLOR_WHITE, (0, 0, 0))
        tsrect = text_surf.get_rect()
        tsrect.right = SURFACE_MAIN.get_rect().right
        tsrect = tsrect.move(-5, 65)
        SURFACE_MAIN.blit(text_surf, tsrect)
        SURFACE_FONT.set_bold(False)


    # update the display
    pygame.display.flip()

def draw_map(map_to_draw):
    SURFACE_MAIN.fill(constants.COLOR_BLACK)

    for x in range(constants.MAP_WIDTH):
        for y in range(constants.MAP_HEIGHT):
            if map_to_draw[x][y].is_wall:
                # draw wall
                SURFACE_MAIN.blit(
                    constants.S_WALL,
                    (x * constants.CELL_WIDTH, y * constants.CELL_HEIGHT)
                )
            elif map_to_draw[x][y].is_healpoint:
                # draw floor, and then healpoint on top of it
                SURFACE_MAIN.blit(
                    constants.S_FLOOR,
                    (x * constants.CELL_WIDTH, y * constants.CELL_HEIGHT)
                )
                SURFACE_MAIN.blit(
                    constants.S_FLOOR_HP,
                    (x * constants.CELL_WIDTH, y * constants.CELL_HEIGHT)
                )
            elif not map_to_draw[x][y].is_void:
                # draw floor
                SURFACE_MAIN.blit(
                    constants.S_FLOOR,
                    (x * constants.CELL_WIDTH, y * constants.CELL_HEIGHT)
                )


#      _________    _____   ____
#     / ___\__  \  /     \_/ __ \
#    / /_/  > __ \|  Y Y  \  ___/
#    \___  (____  /__|_|  /\___  >
#   /_____/     \/      \/     \/
#
def game_main_loop():
    '''In this function, we loop the main game'''
    game_quit = False

    # player action definition
    player_action = "no-action"

    while not game_quit:
        pygame.time.wait(250)

        # handle player input
        player_action = game_handle_keys()

        if player_action == "QUIT":
            game_quit = True

        # if PLAYER did something (and it's NOT a move), other GAME_OBJECTS
        #  should do something as well
        # ... otherwise, keep waiting
        elif player_action != "no-action" and not player_action == "player-moved":
            for obj in GAME_OBJECTS:
                if obj.ai:
                    obj.ai.take_turn()

        # draw the game
        draw_game()

    # quit the game
    pygame.quit()
    exit()


def game_initialize():
    '''This function initializes the main window, and pygame'''

    global SURFACE_MAIN, GAME_MAP, PLAYER, ENEMY, GAME_OBJECTS, ENEMIES_TO_DEFEAT, SURFACE_PLAYER_DATA, GAME_FINISHED, GAME_F_WIN
    ENEMIES_TO_DEFEAT = 10
    GAME_FINISHED = False
    GAME_F_WIN = False

    # initialize pygame
    pygame.init()

    # init fonts
    pygame.font.init()

    SURFACE_MAIN = pygame.display.set_mode(
        (constants.MAP_WIDTH * constants.CELL_WIDTH,
         constants.MAP_HEIGHT * constants.CELL_HEIGHT)
    )

    # set window title
    pygame.display.set_caption("The Wizards of E'khek")

    # set window icon
    pygame.display.set_icon(constants.S_PLAYER)

    GAME_MAP = map_create()

    creature_com1 = com_Creature("Player", hp = 20, ap = 10)
    PLAYER = obj_Actor(
        10, 17, "PlayerActor", constants.S_PLAYER, creature = creature_com1
    )

    '''
    creature_com2 = com_Creature("Enemy", defeat_function = defeat_monster)
    ai_com = ai_Test()
    ENEMY = obj_Actor(
        15, 15, "troll", constants.S_ENEMY, creature = creature_com2, ai = ai_com
    )
    GAME_OBJECTS = [PLAYER, ENEMY]
    '''

    GAME_OBJECTS = [PLAYER]


def game_handle_keys():
    # get player input
    events = pygame.event.get()

    # process input
    for event in events:
        if event.type == pygame.QUIT:
            return "QUIT"

        # KEYDOWN is the keypress event
        if event.type == pygame.KEYDOWN:
            # regardless of using if or elif, diagonal moves (say, K_DOWN and
            # K_RIGHT pressed simultaneously) are two-turn actions
            if not GAME_FINISHED:
                if PLAYER.creature.in_fight:
                    if event.key == pygame.K_h:
                        PLAYER.creature.hit_enemy()
                        # returning "player-attack" to invite AI action
                        return "player-attack"

                    if event.key == pygame.K_a:
                        PLAYER.creature.run_away()
                else:
                    if event.key == pygame.K_UP:
                        PLAYER.creature.move(0, -1)
                        return "player-moved"

                    if event.key == pygame.K_DOWN:
                        PLAYER.creature.move(0, 1)
                        return "player-moved"

                    if event.key == pygame.K_LEFT:
                        PLAYER.creature.move(-1, 0)
                        return "player-moved"

                    if event.key == pygame.K_RIGHT:
                        PLAYER.creature.move(1, 0)
                        return "player-moved"

            else:
                # accept only:
                #  R - restart
                #  Q - quit
                if event.key == pygame.K_r:
                    game_initialize()

                if event.key == pygame.K_q:
                    return "QUIT"

            # TODO: handle dumb input here



    return "no-action"

def game_finish(win):
    global GAME_FINISHED, GAME_F_WIN
    GAME_FINISHED = True
    if win:
        print "game won!"
        GAME_F_WIN = True
    else:
        print "game defeated!"
        GAME_F_WIN = False



#############################################################
###################################################   #######
###############################################   /~\   #####
############################################   _- `~~~', ####
##########################################  _-~       )  ####
#######################################  _-~          |  ####
####################################  _-~            ;  #####
##########################  __---___-~              |   #####
#######################   _~   ,,                  ;  `,,  ##
#####################  _-~    ;'                  |  ,'  ; ##
###################  _~      '                    `~'   ; ###
############   __---;                                 ,' ####
########   __~~  ___                                ,' ######
#####  _-~~   -~~ _                               ,' ########
##### `-_         _                              ; ##########
#######  ~~----~~~   ;                          ; ###########
#########  /          ;                        ; ############
#######  /             ;                      ; #############
#####  /                `                    ; ##############
###  /                                      ; ###############
#                                            ################
if __name__ == '__main__':
    game_initialize()
    game_main_loop()
