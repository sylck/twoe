# **T**he **W**izards **o**f **E**'khek (TWoE)

A roguelike game, a dungeon crawler destined to recreate the artistic style from the glory days of computer gaming.


## Credits
Significant code portions based on The Terrible Programmer's [Roguelike Tutorial](https://www.youtube.com/playlist?list=PLKUel_nHsTQ1yX7tQxR_SQRdcOFyXfNAb) series.

### Walls, floor and the heal point

| Name   | Dungeon Crawl 32x32 tiles                                          |
|--------|--------------------------------------------------------------------|
| Author | Chris Hamons (maintainer), submitted by MedicineStorm              |
| URL    | https://opengameart.org/content/dungeon-crawl-32x32-tiles          |
- wall downscaled to 16x16 and then tiled to 32x32
- reduced brightness and bumped the contrast on floor a bit to make it work together
- heal point altered a bit to look more like it actually does some healing 

### Player and enemy characters

| Name   | A load of overworld 3/4 RPG sprites                                |
|--------|--------------------------------------------------------------------|
| Author | Redshrike (Stephen Challener)                                      |
| URL    | https://opengameart.org/content/a-load-of-overworld-34-rpg-sprites |
